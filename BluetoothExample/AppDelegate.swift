//
//  AppDelegate.swift
//  BluetoothExample
//
//  Created by Gao on 4/4/20.
//  Copyright © 2020 Gao. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        print("Launching App")

        let centralRestorationKeys = launchOptions?[UIApplication.LaunchOptionsKey.bluetoothCentrals] as? NSArray
        print("Restoration keys: \(centralRestorationKeys?.description ?? "empty")")
        return true
    }

}

