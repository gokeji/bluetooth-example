//
//  BluetoothCentralManager.swift
//  BluetoothExample
//
//  Created by Gao on 4/4/20.
//  Copyright © 2020 Gao. All rights reserved.
//

import CoreBluetooth
import Foundation
import ReactiveSwift
import ReactiveCocoa

class BluetoothCentralManager: NSObject, CBCentralManagerDelegate {
    var manager: CBCentralManager!

    let centralManagerState = Signal<CBManagerState, Never>.pipe()
    let discoveredPeripheral = Signal<(PeripheralData), Never>.pipe()
    let peripheralConnectionChanged = Signal<PeripheralConnection, Never>.pipe()

    override init() {
        super.init()
        manager = CBCentralManager(delegate: self, queue: nil, options: [CBCentralManagerOptionRestoreIdentifierKey: "MyCentralRestorationKey"])
    }

    // MARK: - Public API

    func startScan() {
        let servicesToScan: [CBUUID]? = [BluetoothPeripheralManager.serviceIdentifier]
        manager.scanForPeripherals(withServices: servicesToScan, options: nil)
    }

    func stopScan() {
        manager.stopScan()
    }

    func connectPeripheral(_ peripheral: CBPeripheral) {
        manager.connect(peripheral)
    }

    func disconnectPeripheral(_ peripheral: CBPeripheral) {
        manager.cancelPeripheralConnection(peripheral)
    }

    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        print("centralManagerDidUpdateState: \(central.state.rawValue)")

        centralManagerState.input.send(value: central.state)
    }

    // MARK: - CBCentralManagerDelegate

    func centralManager(_ central: CBCentralManager, willRestoreState dict: [String : Any]) {
        print("Central Manager Restored")
        let peripherals: [CBPeripheral] = dict[
            CBCentralManagerRestoredStatePeripheralsKey] as? [CBPeripheral] ?? []
        if peripherals.count > 1 {
            print("Warning: willRestoreState called with >1 connection")
        }
        // We have a peripheral supplied, but we can't touch it until
        // `central.state == .poweredOn`, so we store it in the state
        // machine enum for later use.
        if let peripheral = peripherals.first {
            switch peripheral.state {
            case .connecting: // I've only seen this happen when
                // re-launching attached to Xcode.
                print("connecting to Peripheral")

            case .connected: // Store for connection / requesting
                // notifications when BT starts.

                print("connected to Peripheral")
            default: break
            }
        }
    }

    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        print("\(Date().description): \(peripheral.identifier) - \(peripheral.name ?? "unknown")")
//        print(advertisementData)
//        print("Rssi: \(RSSI)")

        discoveredPeripheral.input.send(
            value: PeripheralData(
                peripheral: peripheral,
                rssi: RSSI,
                advertisementData: advertisementData
            )
        )
    }

    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("connected Peripheral \(peripheral)")
        peripheralConnectionChanged.input.send(value: PeripheralConnection(peripheral: peripheral, isConnected: true))
    }

    func centralManager(_ central: CBCentralManager, connectionEventDidOccur event: CBConnectionEvent, for peripheral: CBPeripheral) {
        print("connection event occured: \(event)")
    }

    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        print("failed to connect to peripheral")
    }

    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        print("disconnect from peripheral")
        peripheralConnectionChanged.input.send(value: PeripheralConnection(peripheral: peripheral, isConnected: false))
    }
}
