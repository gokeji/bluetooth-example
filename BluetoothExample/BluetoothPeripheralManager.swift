//
//  BluetoothCentralManager.swift
//  BluetoothExample
//
//  Created by Gao on 4/4/20.
//  Copyright © 2020 Gao. All rights reserved.
//

import CoreBluetooth
import Foundation

class BluetoothPeripheralManager: NSObject, CBPeripheralManagerDelegate {
    static let serviceIdentifier: CBUUID = CBUUID(string: "CD19")
    var manager: CBPeripheralManager!
    let myCovidId = "TEST-ID" // Testing the ability to advertise my phone/user's unique ID

    override init() {
        super.init()
        manager = CBPeripheralManager(delegate: self, queue: nil, options: [CBCentralManagerOptionRestoreIdentifierKey: "MyPeripheralRestoreKey"])
    }

    func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
        print("peripheralManagerDidUpdateState: \(peripheral.state.rawValue)")
        if peripheral.state == .poweredOn {
            startService()
        }
    }

    private func startService() {
        // Should this be assigned by backend per account? Or generated in client and remembered
        let serviceUUID = BluetoothPeripheralManager.serviceIdentifier
//        let covidCharacteristicUUID = CBUUID(string: "9C518A48-3D84-4F02-9721-2FD8D7677B15")

//        let myIdentifier = CBMutableCharacteristic(type: covidCharacteristicUUID, properties: .read, value: Data(base64Encoded: myCovidId), permissions: .readable)
        let service = CBMutableService(type: serviceUUID, primary: true)
//        service.characteristics = [myIdentifier]

        manager.add(service)
    }

    func peripheralManager(_ peripheral: CBPeripheralManager, willRestoreState dict: [String : Any]) {
        print("Restored peripheral state: \(dict)")
    }

    func peripheralManager(_ peripheral: CBPeripheralManager, didAdd service: CBService, error: Error?) {
        if let error = error {
            print("Error adding service \(error)")
        }
        startAdvertising(service: service)
    }

    private func startAdvertising(service: CBService) {
        manager.startAdvertising([
            CBAdvertisementDataLocalNameKey: "Contact Tracing Service",
            CBAdvertisementDataServiceUUIDsKey: [service.uuid]
        ])
    }

    func peripheralManagerDidStartAdvertising(_ peripheral: CBPeripheralManager, error: Error?) {
        if let error = error {
            print("Error advertising service \(error)")
        }
    }
}
