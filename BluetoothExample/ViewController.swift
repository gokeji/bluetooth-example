//
//  ViewController.swift
//  BluetoothExample
//
//  Created by Gao on 4/4/20.
//  Copyright © 2020 Gao. All rights reserved.
//

import CoreBluetooth
import ReactiveSwift
import UIKit

struct PeripheralData {
    var peripheral: CBPeripheral
    var rssi: NSNumber
    var advertisementData: [String: Any]
}

struct PeripheralConnection {
    let peripheral: CBPeripheral
    let isConnected: Bool
}

class ViewController: UIViewController {
    @IBOutlet var startScanButton: UIButton!
    @IBOutlet var stopScanButton: UIButton!
    @IBOutlet var scanningLabel: UILabel!
    @IBOutlet var tableView: UITableView!

    let centralManager = BluetoothCentralManager()
    let peripheralManager = BluetoothPeripheralManager()

    var timer: Timer?

    var peripherals = [UUID: PeripheralData]() {
        didSet {
            sortedData = peripherals.map { $0 }.sorted { $0.value.rssi.compare($1.value.rssi) == .orderedDescending }
        }
    }
    var sortedData = [(key: UUID, value: PeripheralData)]()
    var peripheralConnections = [UUID: Bool]()

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self

        startScanButton.isEnabled = false
        stopScanButton.isEnabled = false

        centralManager.centralManagerState.output
            .take(during: reactive.lifetime)
            .observe(on: UIScheduler())
            .observeValues({ [weak self] in
                self?.startScanButton.isEnabled = $0 == .poweredOn
                self?.stopScanButton.isEnabled = $0 == .poweredOn
            })

        centralManager.discoveredPeripheral.output
            .take(during: reactive.lifetime)
            .observe(on: UIScheduler())
            .observeValues({ [weak self] in
                self?.addPeripheralData($0)
            })

        centralManager.peripheralConnectionChanged.output
            .take(during: reactive.lifetime)
            .observe(on: UIScheduler())
            .observeValues({ [weak self] in
                let uuid = $0.peripheral.identifier
                self?.peripheralConnections[uuid] = $0.isConnected
                self?.tableView.reloadData()

                if $0.isConnected {
                    $0.peripheral.delegate = self
                    $0.peripheral.discoverServices([BluetoothPeripheralManager.serviceIdentifier])
//                    $0.peripheral.discoverServices(nil)
                }
            })
    }

    private func addPeripheralData(_ data: PeripheralData) {
        let uuid = data.peripheral.identifier
        peripherals[uuid] = data
        tableView.reloadData()
    }

    @IBAction func tappedStartScan(_ sender: Any) {
        scanningLabel.text = "Scanning..."

        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [weak self] timer in
            print("Scanning")
            self?.centralManager.startScan()
        }
    }

    @IBAction func tappedStopScan(_ sender: Any) {
        centralManager.stopScan()
        scanningLabel.text = "Scanning Stopped"

        timer?.invalidate()
        timer = nil
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = sortedData[indexPath.row].value
        let peripheral = data.peripheral
        let isConnected = peripheralConnections[peripheral.identifier] ?? false

        if !isConnected {
            centralManager.connectPeripheral(peripheral)
        } else {
            centralManager.disconnectPeripheral(peripheral)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        peripherals.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = sortedData[indexPath.row].value
        let peripheral = data.peripheral
        let isConnected: Bool = peripheralConnections[peripheral.identifier] ?? false

        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cell")

        let nameCharacteristic = data.advertisementData[CBAdvertisementDataLocalNameKey] as? NSString
        let uuidCharacteristic = data.advertisementData[CBAdvertisementDataServiceUUIDsKey] as? [CBUUID]

        var title = peripheral.name ?? "unknown"
        if let nameCharacteristic = nameCharacteristic {
            title += " [\(nameCharacteristic)]"
        }
        cell.textLabel?.text = title

        var subtitle = "RSSI: \(data.rssi)"
        if let uuidCharacteristic = uuidCharacteristic?.first {
            subtitle += " - \(uuidCharacteristic)"
        }
        cell.detailTextLabel?.text = subtitle

        cell.accessoryType = isConnected ? .checkmark : .none
        return cell
    }

}

extension ViewController: CBPeripheralDelegate {
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
//        peripherals[peripheral.identifier]?.update(peripheral: peripheral, rssi: nil)
//        tableView.reloadData()

        print("Discovered Contact Tracer Service: \(peripheral.services?.description ?? "No Services")")
    }
}
